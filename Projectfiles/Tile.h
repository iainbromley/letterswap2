//
//  Tile.h
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class PlayfieldLayer;

typedef enum {
    kTileAnyType = 0,
    kTile01,
    kTile02,
    kTile03,
    kTile04,
    kTile05,
    kTile06,
    kTile07,
    kTile08,
    kTile09,
    kTile10,
    kTile11,
    kTile12,
    kTile13,
    kTile14,
    kTile15,
    kTile16,
    kTile17,
    kTile18,
    kTile19,
    kTile20,
    kTile21,
    kTile22,
    kTile23,
    kTile24,
    kTile25,
    kTile26
} TileType;

typedef enum {
    kTileIdle = 100,
    kTileMoving,
    kTileScoring,
    kTileNew
} TileState;

@interface Tile : CCSprite {
    NSInteger _rowNum; 
    NSInteger _colNum; 
    
    TileType _tileType; // enum value of the tile
    
    TileState _tileState; 
    
    PlayfieldLayer *gameLayer; 
    
}

@property (nonatomic, assign) NSInteger rowNum;
@property (nonatomic, assign) NSInteger colNum;
@property (nonatomic, assign) TileType tileType;
@property (nonatomic, assign) TileState tileState;
@property (nonatomic, assign) PlayfieldLayer *gameLayer;

-(BOOL) isTileSameAs:(Tile*)otherTile;
-(BOOL) isTileInSameRow:(Tile*)otherTile;
-(BOOL) isTileInSameColumn:(Tile*)otherTile;
-(BOOL) isTileBeside:(Tile*)otherTile;

-(void) highlightTile;
-(void) stopHighlightTile;

- (BOOL) containsTouchLocation:(CGPoint)pos;
@end

