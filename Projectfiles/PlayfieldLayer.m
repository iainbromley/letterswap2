//
//  PlayfieldLayer.m
//


#import "PlayfieldLayer.h"

NSMutableString *word;

@implementation PlayfieldLayer


-(id) init {
	
    if (self == [super init]) {
        
        //self.isTouchEnabled = YES;
        
        size = [[CCDirector sharedDirector] winSize];
        
        //glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        
        CCSprite *bg = [CCSprite spriteWithFile:
                        @"match3bg.png"];
        [bg setPosition:ccp(size.width/2,size.height/2)];
        [self addChild:bg z:0];
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache]
         addSpriteFramesWithFile:@"lettersheet3.plist"];
        matchsheet = [CCSpriteBatchNode batchNodeWithFile:
                      @"lettersheet3.png" capacity:54];
        
        [self addChild:matchsheet z:1];
        
        backButton = [CCSprite spriteWithSpriteFrameName:
                      @"backbutton.png"];
        [backButton setAnchorPoint:ccp(0,0)];
        //[backButton setScale:0.7];
        [backButton setPosition:ccp(10, 250)];
        [matchsheet addChild:backButton];
        
        freezeButton = [CCSprite spriteWithSpriteFrameName: @"snowflake.png"];
        [freezeButton setAnchorPoint:ccp(0,0)];
        [freezeButton setPosition:ccp(20,170)];
        [matchsheet addChild:freezeButton];
       
        
        bombButton = [CCSprite spriteWithSpriteFrameName: @"bomb.png"];
        [bombButton setAnchorPoint:ccp(0,0)];
        [bombButton setPosition:ccp(20,120)];
        [matchsheet addChild:bombButton];
        
        swapButton = [CCSprite spriteWithSpriteFrameName: @"swap.png"];
        [swapButton setAnchorPoint:ccp(0,0)];
        [swapButton setPosition:ccp(20,70)];
        [matchsheet addChild:swapButton];
        
        boardRows = 6;
        boardColumns = 6;
        boardOffsetWidth = 80;
        boardOffsetHeight = 0;
        padWidth = 4;
        padHeight = 4;
        tileSize = CGSizeMake(40,40);
        
        totalTilesAvailable = 26;
        
        tilesInPlay = [[NSMutableArray alloc] init];
        tilesInPlaysort = [[NSMutableArray alloc] init];
        tileMatches = [[NSMutableArray alloc] init];
        tilesTouched = [[NSMutableArray alloc] init];
        
        playerScore = 0;
        lastWord = @"";
        isGameOver = NO;
        isSuperSwapOn = NO;
        freezePowerUp = 5;
        bombPowerUp = 5;
        swapPowerUp = 5;
        
        
        [self generateScoreDisplay];
        [self generatePowerUpDisplay];
        [self generateLastWordDisplay];
        
        isTimerOn = YES;
        [self generateTimerDisplay];
        startingTimerValue = 60; //seconds
        currentTimerValue = startingTimerValue;
        
        [self generatePlayfield];
        //[self checkDictionary];
        
        //[self generateTestingPlayfield];  ///debugging
        
        //[self checkMovesRemaining];
        
        [self scheduleUpdate];

    }
    return self;
}

-(void) dealloc {
    
    //self.isTouchEnabled = NO;
    
    [tilesInPlay removeAllObjects];
    tilesInPlay = nil;
    
    [tileMatches removeAllObjects];
    tileMatches = nil;
    
    [tilesTouched removeAllObjects];
    tilesTouched = nil;
    
    //[super dealloc];
}

-(void) update:(ccTime)dt {
    
    tilesMoving = NO;
    
    // any moving tiles
    for (Tile *aTile in tilesInPlay) {
        if (aTile.tileState == kTileMoving) {
            tilesMoving = YES;
            break;
        }
    }
    
    //[self checkDictionary];
    
    if (checkMatches && tilesInPlay.count > 35) {
        [self checkMove];
        //[self checkMovesRemaining];
        checkMatches = NO;
    }
    
    //[self checkMove];
    
    
    // refill board error on vertical matches
    if ([tilesInPlay count] < boardRows * boardColumns &&
        tilesMoving == NO) {
        //[self addTilesToFillBoard];
    }
    
    if (isTimerOn == YES){
    currentTimerValue = currentTimerValue - dt;
    [timerDisplay setPercentage:(currentTimerValue /
                                 startingTimerValue) * 100];
    }
    
    
    if (currentTimerValue <= 0) {
        [self unscheduleUpdate];
        isGameOver = YES;
        [self gameOver];
    }
}



#pragma mark Generate Tile Grid
-(void) generatePlayfield {
    // Iterate through all rows and columns
    for (int row = 1; row <= boardRows; row++) {
        for (int col = 1; col <= boardColumns; col++) {
            [self generateTileForRow:row andColumn:col
                              ofType:kTileAnyType];
        }
    }
    for (Tile *aTile in tilesInPlay){
        //NSLog(@"%@", aTile);
}

    // check matches before start
    [self fixStartingMatches];
    
    for (Tile *aTile in tilesInPlay) {
        [aTile setTileState:kTileIdle];
        [matchsheet addChild:aTile];
    }
    
}


-(void) fixStartingMatches {
    
    //[self checkForMatchesOfType2:kTileNew];
    //[self checkDictionary];
    
    if ([tileMatches count] > 0) {
        
        Tile *aTile = [tileMatches objectAtIndex:0];
        
        [self generateTileForRow:[aTile rowNum] andColumn:
         [aTile colNum] ofType:kTileAnyType];
        
        [tilesInPlay removeObject:aTile];
        [tileMatches removeObject:aTile];
        
        [self fixStartingMatches];
    }
}


-(Tile*) generateTileForRow:(NSInteger)rowNum
                  andColumn:(NSInteger)colNum ofType:(TileType)newType {
    
    TileType tileNum;
    
    // realistic SCRABBLE(R) selection of tiles
    NSArray *selection = @[@"A", @"A", @"A", @"A", @"A", @"A", @"A", @"A", @"A", \
                           @"B", @"B", \
                           @"C", @"C", \
                           @"D", @"D", @"E", \
                           @"E", @"E", @"E", @"E", @"E", @"E", @"E", @"E", @"E", @"E", @"E", @"E", \
                           @"F", @"F", \
                           @"G", @"G", @"G", \
                           @"H", @"H", \
                           @"I", @"I", @"I", @"I", @"I", @"I", @"I", @"I", @"I", \
                           @"J", \
                           @"K", \
                           @"L", @"L", @"L", @"L", \
                           @"M", @"M", \
                           @"N", @"N", @"N", @"N", @"N", @"N", \
                           @"O", @"O", @"O", @"O", @"O", @"O", @"O", @"O", \
                           @"P", @"P", \
                           @"Q", \
                           @"R", @"R", @"R", @"R", @"R", @"R", \
                           @"S", @"S", @"S", @"S", \
                           @"T", @"T", @"T", @"T", @"T", @"T", \
                           @"U", @"U", @"U", @"U", \
                           @"V", @"V", \
                           @"W", @"W", \
                           @"X", \
                           @"Y", @"Y", \
                           @"Z"];
    
    if (newType == kTileAnyType) {
        // randomize the tile, old version
        //tileNum = (arc4random() % totalTilesAvailable) + 1;
        int index = 0;
        NSString *numberletter = @"";
        int number = 0;
        index = arc4random() % ([selection count]);
        numberletter = [selection objectAtIndex: index];
        //NSLog (numberletter);
        //NSLog(numberletter);
        number = [numberletter characterAtIndex:0] - 64;
        //number = numberletter - 64;
        tileNum = number;
        //NSLog(@"%i", tileNum);
    } else {
        // if passed another value, use that
        tileNum = newType;
    }
    
    NSString *spritename = [NSString stringWithFormat:
                            @"tile%i.png", tileNum];
    
    
    Tile *thisTile = [Tile
                      spriteWithSpriteFrameName:spritename];
    
    [thisTile setRowNum:rowNum];
    [thisTile setColNum:colNum];
    [thisTile setTileType:(TileType)tileNum];
    [thisTile setTileState:kTileNew];
    [thisTile setGameLayer:self];
    
    [thisTile setPosition:[self positionForRow:rowNum
                                     andColumn:colNum]];
    
    //[tilesInPlay addObject:thisTile];
    [tilesInPlay insertObject:thisTile atIndex:((thisTile.rowNum-1)*6+thisTile.colNum-1)];
    
    // added to the tilesInPlay array
    // NOT been added to the layer yet.
    return thisTile;
}

-(void) addTileForRow:(NSInteger)rowNum
            andColumn:(NSInteger)colNum
               ofType:(TileType)newType {
    
    Tile *thisTile = [self generateTileForRow:rowNum
                                    andColumn:colNum ofType:newType];
    
    //[tilesInPlay replaceObjectAtIndex:((thisTile.rowNum-1)*6+aTile.colNum-1) withObject:aTile];
    //[tilesInPlay addObject:thisTile];
    [tilesInPlay insertObject:thisTile atIndex:((thisTile.rowNum-1)*6+thisTile.colNum-1)];
    
    // reset the tile above the screen
    [thisTile setPosition:ccpAdd(thisTile.position,
                                 ccp(0,size.height))];
    
    [self addChild:thisTile];
    
    // put into the correct position
    [self moveToNewSlotForTile:thisTile];
    
}


//UNused method, uncomment if using checkDictionary
/*
 -(void) removeTileForRow:(NSInteger)row andColumn:(NSInteger)col {
 Tile *aTile = [Tile alloc];
 [aTile setRowNum:row];
 [aTile setColNum:col];
 //thisTile = [row][col];
 [self animateTileRemoval:aTile];
 }
 */

#pragma mark Tile Manipulation
-(void)swapTile:(Tile*)aTile withTile:(Tile*)bTile{
    NSInteger tempRowNumA;
    NSInteger tempColNumA;
    
    // Stop the highlight
    [aTile stopHighlightTile];
    [bTile stopHighlightTile];
    
    // Grab the location of aTile
    tempRowNumA = [aTile rowNum];
    tempColNumA = [aTile colNum];
    
    // Set the aTile to the values from bTile
    [aTile setRowNum:[bTile rowNum]];
    [aTile setColNum:[bTile colNum]];
    
    // Set the bTile to the values from the aTile temp vars
    [bTile setRowNum:tempRowNumA];
    [bTile setColNum:tempColNumA];
    
    // Move tiles around the array.
    [tilesInPlay exchangeObjectAtIndex:((aTile.rowNum-1)*6+aTile.colNum-1) withObjectAtIndex:((bTile.rowNum-1)*6+bTile.colNum-1)];
    //[tilesInPlay exchangeObjectAtIndex:1 withObjectAtIndex:2];
    
    // Move the tiles
    [self moveToNewSlotForTile:aTile];
    [self moveToNewSlotForTile:bTile];
}

-(void) moveToNewSlotForTile:(Tile*)aTile {
    [aTile setTileState:kTileMoving];
    
    CCMoveTo *moveIt = [CCMoveTo
                        actionWithDuration:0.2
                        position:[self positionForRow:[aTile rowNum]
                                            andColumn:[aTile colNum]]];
    CCCallFuncND *tileAtRest = [CCCallFuncND
                                actionWithTarget:self
                                selector:@selector(tileIsAtRest:) data:(__bridge void *)(aTile)];
    [aTile runAction:[CCSequence actions:moveIt, tileAtRest, nil]];
    //[tilesInPlay replaceObjectAtIndex:((aTile.rowNum-1)*6+aTile.colNum-1) withObject:aTile];
}

-(void) tileIsAtRest:(Tile*)aTile {
    // Reset the tile's state to Idle
    [aTile setTileState:kTileIdle];
    // Identify that we need to check for matches
    checkMatches = YES;
}

-(void) resetTilePosition:(Tile*)aTile {
    // put tile back to position after the tile stops moving
    [aTile setPosition:[self positionForRow:[aTile rowNum]
                                  andColumn:[aTile colNum]]];
}

-(void) animateTileRemoval:(Tile*)aTile {
    CCCallFunc *updateScore = [CCCallFunc
                               actionWithTarget:self
                               selector:@selector(incrementScore)];
    CCCallFunc *addTime = [CCCallFunc
                           actionWithTarget:self
                           selector:@selector(addTimeToTimer)];
    CCMoveBy *moveUp = [CCMoveBy actionWithDuration:0.3
                                           position:ccp(0,5)];
    CCFadeOut *fade = [CCFadeOut actionWithDuration:0.2];
    CCCallFuncND *removeTile = [CCCallFuncND
                                actionWithTarget:self
                                selector:@selector(removeTile:) data:(__bridge void *)(aTile)];
    
    [aTile runAction:[CCSequence actions:
                      updateScore, addTime,
                      moveUp, fade,
                      removeTile, nil]];
    //[self addTilesToFillBoard];
}

//Future method....
/*
 -(void) changeTileFace:(Tile*)aTile {
 [aTile setDisplayFrame:[[CCSpriteFrameCache
 sharedSpriteFrameCache]
 spriteFrameByName:@"backbutton.png"]];
 }
 */

-(void) removeTile:(Tile*)aTile {
    
    
    
    
         /*
         Tile *thisTile3 = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+3)*6+aTile.colNum-1)];
         if (aTile.colNum == thisTile3.colNum && aTile.rowNum < thisTile3.rowNum) {
         [tilesInPlay replaceObjectAtIndex:(((aTile.rowNum-1)+2)*6+aTile.colNum-1) withObject:thisTile3];
         [thisTile setRowNum:thisTile3.rowNum - 2];
         [self moveToNewSlotForTile:thisTile];
         //Tile *thisTile = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+index3)*6+aTile.colNum-1)];
         //[self fillHolesFromTile:thisTile];
         
          Tile *thisTile3 = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+3))*6+aTile.colNum-1)];
          if (aTile.colNum == thisTile3.colNum && aTile.rowNum < thisTile3.roNum){
          [tilesInPlay replaceObjectAtIndex:(((aTile.rowNum-1)+2)*6+aTile.colNum-1) withObject:thisTile3];
          [thisTile setRowNum:thisTile3.rowNum -2];
          [self moveToNewSlotForTile:thisTile];
          //Tile *thisTile=[tilesInPlay objectAtIndex:(((aTile.rowNum-1)+3))*6+aTile.colNum-1)];
          //[self fillHolesFromTile:thisTile];
          
         }
         */
        
    [tilesInPlay removeObject:aTile];
    
    [aTile setTileState:kTileScoring];
    [self fillHolesFromTile:aTile];
    [aTile removeFromParentAndCleanup:YES];
    checkMatches = YES;
    //[self addTilesToFillBoard];
}


#pragma mark Scoring

-(void) generateScoreDisplay {
    CCLabelTTF *scoreTitleLbl = [CCLabelTTF
                                 labelWithString:@"SCORE" fontName:@"Verdana-Bold" fontSize:20];
    [scoreTitleLbl setPosition:ccpAdd([self scorePosition],ccp(0,20))];
    [self addChild:scoreTitleLbl z:2];
    
    scoreLabel = [CCLabelTTF labelWithString:[NSString
                                              stringWithFormat:@"%i", playerScore]
                                    fontName:@"Verdana-Bold" fontSize:18];
    [scoreLabel setPosition:[self scorePosition]];
    [self addChild:scoreLabel z:3];
}

-(void) incrementScore {
    playerScore++;
    [self updateScore];
}

-(void) updateScore {
    [scoreLabel setString:[NSString stringWithFormat:@"%i", playerScore]];
}

-(void)generateSwapsLeftDisplay {
    CCLabelTTF *swapsLeftTitleLabel = [CCLabelTTF labelWithString:@"Moves Remaining" fontName:@"Verdana-Bold" fontSize:10];
    [swapsLeftTitleLabel setPosition:ccpAdd([self swapPosition], ccp(0,20))];
    [self addChild:swapsLeftTitleLabel z:4];
    swapsLeftLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", swapsLeft] fontName:@"Verdana-Bold" fontSize:18];
    [swapsLeftLabel setPosition:[self swapPosition]];
    [self addChild:swapsLeftLabel];
}

-(void)updateSwapsLeft {
    [swapsLeftLabel setString:[NSString stringWithFormat:@"%i",swapsLeft]];
}


-(void)generateLastWordDisplay {
    CCLabelTTF *wordTitleLbl = [CCLabelTTF labelWithString:@"Previous Word" fontName:@"Verdana-Bold" fontSize:12];
    [wordTitleLbl setPosition:ccpAdd([self wordPosition],ccp(0,20))];
     [self addChild:wordTitleLbl z:4];
    
    wordLabel = [CCLabelTTF labelWithString:lastWord fontName:@"Verdana-Bold" fontSize:24];
     [wordLabel setPosition:[self wordPosition]];
    [self addChild:wordLabel z:5];
}

-(void)generatePowerUpDisplay {
    freezePowerUpLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", freezePowerUp]
                                                        fontName:@"Verdana-Bold" fontSize:36];
    [freezePowerUpLabel setPosition:ccp(80,190)];
    [self addChild:freezePowerUpLabel z:12];
    
    bombPowerUpLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", bombPowerUp]
                                                      fontName:@"Verdana-Bold" fontSize:36];
    swapPowerUpLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", swapPowerUp]
                                                      fontName:@"Verdana-Bold" fontSize:36];
    
    [bombPowerUpLabel setPosition:ccp(80,140)];
    [self addChild:bombPowerUpLabel z:13];
    [swapPowerUpLabel setPosition:ccp(80,100)];
    [self addChild:swapPowerUpLabel z:14];
}

-(void)updatePowerUpDisplay {
    [freezePowerUpLabel setString:[NSString stringWithFormat:@"%i", freezePowerUp]];
    [bombPowerUpLabel setString:[NSString stringWithFormat:@"%i", bombPowerUp]];
    [swapPowerUpLabel setString:[NSString stringWithFormat:@"%i", swapPowerUp]];
}

-(void)updateLastWord {
    [wordLabel setString:lastWord];
}


#pragma mark Timer & Game Over
-(void) generateTimerDisplay {
    
    CCSprite *timerFrame = [CCSprite
                            spriteWithFile:@"timer2.png"];
    [timerFrame setPosition:[self timerPosition]];
    [self addChild:timerFrame z:8];
    
    CCSprite *timerSprite = [CCSprite
                             spriteWithFile:@"timer_back.png"];
    
    timerDisplay = [CCProgressTimer
                    progressWithSprite:timerSprite];
    [timerDisplay setPosition:[self timerPosition]];
    [timerDisplay setType:kCCProgressTimerTypeRadial];
    [self addChild:timerDisplay z:4];
    [timerDisplay setPercentage:100];
}

-(void) addTimeToTimer {
    currentTimerValue = currentTimerValue + 1;
    
    // If out of bounds exception is thrown, set to max
    if (currentTimerValue > startingTimerValue) {
        currentTimerValue = startingTimerValue;
    }
    
}


-(void) gameOver {
    CCLabelTTF *gameOverLabel = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Verdana-Bold" fontSize:60];
    [gameOverLabel setPosition:ccp(size.width/2,
                                   size.height/2)];
    [self addChild:gameOverLabel z:50];
    
    // Oooh.... drop shadow test
    CCLabelTTF *gameOverLabelShadow = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Verdana-Bold" fontSize:60];
    [gameOverLabelShadow setPosition:ccp(size.width/2 - 4,
                                         size.height/2 - 4)];
    [gameOverLabelShadow setColor:ccBLACK];
    [self addChild:gameOverLabelShadow z:49];
}

#pragma mark Check After Move Is Made
-(void) checkMove {
    [self checkForMatchesOfType3:kTileIdle];
    //[self checkDictionary];
    
    if ([tileMatches count] > 0) {
        for (Tile *aTile in tileMatches) {
            if (aTile.tileState != kTileScoring) {
                [self animateTileRemoval:aTile];
            }
        }
        // matches processed, clear array
        [tileMatches removeAllObjects];
        // If we have any selected/touched tiles, we must
        // have made an incorrect move
    } else if ([tilesTouched count] > 0) {
        // If there was only one tile, grab it
        Tile *aTile = [tilesTouched objectAtIndex:0];
        
        
        // If we had 2 tiles in the touched array
        if ([tilesTouched count] == 2) {
            // Grab the second tile
            //Tile *bTile = [tilesTouched objectAtIndex:1];
            // Swap them back to their original slots
            //[self swapTile:aTile withTile:bTile];
        } else {
            // If we only had 1 tile, stop highlighting it
            [aTile stopHighlightTile];
        }
        
    }
    // Touches were processed.  Clear the touched array.
    [tilesTouched removeAllObjects];
    
    //[self addTilesToFillBoard];
    
}


#pragma mark Tile Dropping
-(void) fillHolesFromTile:(Tile*)aTile {
    /*
    for (Tile *thisTile in tilesInPlay) {
        // if tile is in the same column and above the current matching tile, reset the position down, to fill the hole
        if (aTile.colNum == thisTile.colNum && aTile.rowNum < thisTile.rowNum) {
            // Set thisTile to drop down one row
            //Tile *thisTile = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+1)*6+aTile.colNum-1)];
            //[tilesInPlay replaceObjectAtIndex:((aTile.rowNum-1)*6+aTile.colNum-1) withObject:thisTile];
            [thisTile setRowNum:thisTile.rowNum - 1];
            [self moveToNewSlotForTile:thisTile];
            
        }
    }
    */
    ///int startingvalue = ((aTile.rowNum-1)*6+aTile.colNum-1)+6;
    
    for (int startingvalue = ((aTile.rowNum-1)*6+aTile.colNum-2)+6;startingvalue < 35;(startingvalue+=6)) {
        NSLog (@"@%i", startingvalue);
        Tile *thisTile = [tilesInPlay objectAtIndex:startingvalue];
        [tilesInPlay replaceObjectAtIndex:(startingvalue - 6) withObject:thisTile];
        [thisTile setRowNum:thisTile.rowNum-1];
        [thisTile setColNum:thisTile.colNum];
        [self moveToNewSlotForTile:thisTile];
    }
    [self addTileForRow:6 andColumn:aTile.colNum
                 ofType:kTileAnyType];
    
    NSLog(@"%i", tilesInPlay.count);
    //[self addTilesToFillBoard];
    
   /*
    for (int index3 = 1; index3<6; index3++) {
        Tile *thisTile = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+1)*6+aTile.colNum-1)];
        NSLog( @"%i", (((aTile.rowNum-1)+2)*6+aTile.colNum-1));
        if (aTile.colNum == thisTile.colNum && aTile.rowNum < thisTile.rowNum) {
            [tilesInPlay replaceObjectAtIndex:((aTile.rowNum-1)*6+aTile.colNum-1) withObject:thisTile];
            [thisTile setRowNum:thisTile.rowNum - 1];
            [self moveToNewSlotForTile:thisTile];
            //Tile *thisTile = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+index3)*6+aTile.colNum-1)];
            //[self fillHolesFromTile:thisTile];
            
        }
    }
    for (int index4 = 1; index4<6; index4++) {
        Tile *thisTile2 = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+2)*6+aTile.colNum-1)];
        NSLog( @"%i", (((aTile.rowNum-1)+2)*6+aTile.colNum-1));
        if (aTile.colNum == thisTile2.colNum && aTile.rowNum < thisTile2.rowNum) {
            [tilesInPlay replaceObjectAtIndex:(((aTile.rowNum-1)+1)*6+aTile.colNum-1) withObject:thisTile2];
            [thisTile2 setRowNum:thisTile2.rowNum - 2];
            [self moveToNewSlotForTile:thisTile2];
            //Tile *thisTile = [tilesInPlay objectAtIndex:(((aTile.rowNum-1)+index3)*6+aTile.colNum-1)];
            //[self fillHolesFromTile:thisTile];
            
        }
    }
    */
  

}


-(void) addTilesToFillBoard {
    for (int i = 1; i <= boardRows; i++) {
        for (int j = 1; j <= boardColumns; j++) {
            
            BOOL missing = YES;
            
            for (Tile *aTile in tilesInPlay) {
                if (aTile.rowNum == i && aTile.colNum == j
                    && aTile.tileState != kTileScoring) {
                    missing = NO;
                }
            }
            
            if (missing) {
                [self addTileForRow:i andColumn:j
                             ofType:kTileAnyType];
            }
        }
    }
    //2nd check
    checkMatches = YES;
}


#pragma mark Match Checking (actual board)

-(void) checkForMatchesOfType:(TileType)desiredTileState {
    word = [[NSMutableString alloc]init];
    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
    int count = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0;
    // desiredTileState parameter to checks for
    // kTileIdle or kTileNew, depending on whether the
    // game is in play
    //[self checkDictionary];
    // horizontal matches
    //NSLog(@"I am in the method.");
    
    for (Tile *aTile in tilesInPlaysort) {
        tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
        //Tile *aTile tilesInPlaysort
        // Let's grab the first tile
        //if (aTile.tileState == desiredTileState) {
        //NSLog(@"I am in the desired state");
        //NSLog(aTile);
        //NSLog(@"%@", aTile.tileType);
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        //NSLog(word);
        //[word setString:@""];
        
        for (Tile *bTile in tilesInPlaysort) {
           temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
            //NSLog(@"Checking the 2nd tile");
            if ([aTile isTileInSameRow:bTile] &&
                aTile.colNum == bTile.colNum - 1 )
                // &&bTile.tileState == desiredTileState) {
            {
                NSString *tilenumber = [NSString stringWithFormat: @"%02d", bTile.tileType];
                int value = [tilenumber intValue];
                [self numbertoletterconvert:value];
                tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                //NSLog(word);
                //[word setString:@""];
                // looking for a 3rd in a row
            }
            else
            {
                [temptilesInPlaysort removeObject:bTile];
                //[tilesInPlaysort removeAllObjects];
                tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                if ([tilesInPlaysort count] == 0){
                    break;
                }
                count++;
                 if (count > 54321){
                 [word setString:@""];
                 count = 0;
                 break;
                 }
                 
                continue;
                
            }
            for (Tile *cTile in tilesInPlaysort) {
                temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
                //NSLog(@"Checking the 3rd tile");
                if ([aTile isTileInSameRow:cTile] &&
                    aTile.colNum == cTile.colNum - 2 )
                    // &&bTile.tileState == desiredTileState) {
                {
                    NSString *tilenumber = [NSString stringWithFormat: @"%02d", cTile.tileType];
                    int value = [tilenumber intValue];
                    [self numbertoletterconvert:value];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    //NSLog(word);
                    //[word setString:@""];
                    // looking for a 3rd in a row
                }
                else
                {
                    [temptilesInPlaysort removeObject:cTile];
                    //[tilesInPlaysort removeAllObjects];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                    if ([tilesInPlaysort count] == 0){
                        break;
                    }
                     count2++;
                     if (count2 > 54321){
                     [word setString:@""];
                     count2 = 0;
                     break;
                     }
                     
                    continue;
                    
                }
                for (Tile *dTile in tilesInPlaysort) {
                    temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
                    //NSLog(@"Checking the 4th tile");
                    if ([aTile isTileInSameRow:dTile] && aTile.colNum == dTile.colNum - 3)
                        //cTile.tileState == desiredTileState) {
                    {
                        NSString *tilenumber = [NSString stringWithFormat: @"%02d", dTile.tileType];
                        int value = [tilenumber intValue];
                        [self numbertoletterconvert:value];
                        if ([aTile isTileInSameRow:dTile] && [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]) {
                            // Add tiles to match array
                            //NSLog(@"I found a word");
                            
                            [self addTileToMatch:aTile];
                            [self addTileToMatch:bTile];
                            [self addTileToMatch:cTile];
                            [self addTileToMatch:dTile];
                            //[self animateTileRemoval:aTile];
                            //[self animateTileRemoval:bTile];
                            //[self animateTileRemoval:cTile];
                            //[self animateTileRemoval:dTile];
                            //NSLog(word);
                            lastWord = word;
                            [self updateLastWord];
                            [word setString:@""];
                            NSLog(@"%i %i %i",count, count2,count3);
                            count = 0;
                            count2 = 0;
                            count3 = 0;
                            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                            break;
                        }
                        break;
                        
                        
                    }
                    else
                    {
                        [temptilesInPlaysort removeObject:dTile];
                        //[tilesInPlaysort removeAllObjects];
                        tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                        if ([tilesInPlaysort count] == 0){
                            break;
                         }
                        count3++;
                         if (count3 > 54321){
                         [word setString:@""];
                         NSLog(@"%i %i %i",count, count2,count3);
                         count = 0;
                         count2 = 0;
                         count3 = 0;
                         break;
                         }
                         
                        
                    }
                }
                
                break;      }
            [word setString:@""];
            break; }
    }
    
    //Column check
    [tilesInPlaysort removeAllObjects];
    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
    NSLog(@"End row check...... COLUMN CHECK BELOW **********");
    
    for (Tile *aTile in tilesInPlaysort) {
        tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
        //if (aTile.tileState == desiredTileState) {
        //NSLog(@"I am in the desired state");
        //NSLog(aTile);
        //NSLog(@"%@", aTile.tileType);
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        //NSLog(word);
        //[word setString:@""];
        
        for (Tile *bTile in tilesInPlaysort) {
            //NSLog(@"Checking the 2nd tile");
            temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
            if ([aTile isTileInSameColumn:bTile] &&
                aTile.rowNum == bTile.rowNum + 1 )
                // &&bTile.tileState == desiredTileState) {
            {
                NSString *tilenumber = [NSString stringWithFormat: @"%02d", bTile.tileType];
                int value = [tilenumber intValue];
                [self numbertoletterconvert:value];
                tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                //NSLog(word);
                //[word setString:@""];
                // looking for a 3rd in a row
            }
            else
            {
                [temptilesInPlaysort removeObject:bTile];
                //[tilesInPlaysort removeAllObjects];
                tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                if ([tilesInPlaysort count] == 0){
                    break;
                }
                count4++;
                 if (count4 > 54321){
                 [word setString:@""];
                 count4 = 0;
                 break;
                 }
                 
                continue;
                
            }
            for (Tile *cTile in tilesInPlaysort) {
                temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
                //NSLog(@"Checking the 3rd tile");
                // If the tile is the same type and state,
                // in the same row, and to the right
                if ([aTile isTileInSameColumn:cTile] &&
                    aTile.rowNum == cTile.rowNum + 2 )
                    // &&bTile.tileState == desiredTileState) {
                {
                    NSString *tilenumber = [NSString stringWithFormat: @"%02d", cTile.tileType];
                    int value = [tilenumber intValue];
                    [self numbertoletterconvert:value];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    //NSLog(word);
                    //[word setString:@""];
                    // looking for a 3rd in a row
                }
                else
                {
                    [temptilesInPlaysort removeObject:cTile];
                    //[tilesInPlaysort removeAllObjects];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                    if ([tilesInPlaysort count] == 0){
                        break;
                    }
                    count5++;
                     if (count5 > 54321){
                     [word setString:@""];
                     count5 = 0;
                     break;
                     }
                     
                    continue;
                    
                }
                for (Tile *dTile in tilesInPlaysort) {
                    temptilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlaysort];
                    //NSLog(@"Checking the 4th tile");
                    if ([aTile isTileInSameColumn:dTile] && aTile.rowNum == dTile.rowNum + 3)
                        //cTile.tileState == desiredTileState) {
                    {
                        NSString *tilenumber = [NSString stringWithFormat: @"%02d", dTile.tileType];
                        int value = [tilenumber intValue];
                        [self numbertoletterconvert:value];
                        [self reverseString:word];
                        if ([aTile isTileInSameColumn:dTile] && [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]) {
                            // Add tiles to match array
                            //NSLog(@"I found a word");
                            
                            [self addTileToMatch:aTile];
                            [self addTileToMatch:bTile];
                            [self addTileToMatch:cTile];
                            [self addTileToMatch:dTile];
                            //[self animateTileRemoval:aTile];
                            //[self animateTileRemoval:bTile];
                            //[self animateTileRemoval:cTile];
                            //[self animateTileRemoval:dTile];
                            lastWord = word;
                            [self updateLastWord];
                            [word setString:@""];
                            NSLog(@"count 4,5,6 %i %i %i",count4, count5,count6);
                            count4 = 0;
                            count5 = 0;
                            count6 = 0;
                            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                            break;
                        }
                        break;
                        
                        
                    }
                    else
                    {
                        [temptilesInPlaysort removeObject:dTile];
                        //[tilesInPlaysort removeAllObjects];
                        tilesInPlaysort = [NSMutableArray arrayWithArray:temptilesInPlaysort];
                        if ([tilesInPlaysort count] == 0){
                            break;
                        }
                        count6++;
                         if (count6 > 54321){
                         [word setString:@""];
                         NSLog(@"count 4,5,6 %i %i %i",count4, count5,count6);
                         count4 = 0;
                         count5 = 0;
                         count6 = 0;
                         break;
                         }
                         
                        
                    }
                }
                
                break;      }
            [word setString:@""];
            break; }
    }
    
    
    [word setString:@""];
    
}


-(void)checkForMatchesOfType2:(TileType)desiredTileState {
    word = [[NSMutableString alloc]init];
    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
    for (int index = [tilesInPlay count]-1; index>-1; index--) {
        Tile *aTile = [tilesInPlaysort objectAtIndex:index];
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        //NSLog(@"%@", aTile);
        for (int index2 = [tilesInPlay count]-1; index2>-1; index2--) {
            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            Tile *bTile = [tilesInPlaysort objectAtIndex:index2];
            if ([aTile isTileInSameRow:bTile] &&
                aTile.colNum == bTile.colNum - 1 )
            {
                NSString *tilenumber = [NSString stringWithFormat: @"%02d", bTile.tileType];
                int value = [tilenumber intValue];
                [self numbertoletterconvert:value];
                tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            }
            else
            {
                [tilesInPlaysort removeObject:bTile];
                //NSLog(@"%i", [tilesInPlaysort count]);
                if ([tilesInPlaysort count] == 0){
                    [word setString:@""];
                    break;
                }
                
                continue;
            }
            for (int index3 = [tilesInPlay count]-1; index3>-1; index3--) {
                //tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                Tile *cTile = [tilesInPlaysort objectAtIndex:index3];
                if ([aTile isTileInSameRow:cTile] &&
                    aTile.colNum == cTile.colNum -2 )
                    // &&bTile.tileState == desiredTileState) {
                {
                    NSString *tilenumber = [NSString stringWithFormat: @"%02d", cTile.tileType];
                    int value = [tilenumber intValue];
                    [self numbertoletterconvert:value];
                    //[tilesInPlaysort removeAllObjects];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    //NSLog(word);
                    //[word setString:@""];
                    // looking for a 3rd in a row
                }
                else
                {
                    [tilesInPlaysort removeObject:cTile];
                    //NSLog(@"%i", [tilesInPlaysort count]);
                    if ([tilesInPlaysort count] == 0){
                        [word setString:@""];
                        break;
                    }
                    
                    continue;
                    
                }
                for (int index4 = [tilesInPlay count]-1; index4>-1; index4--) {
                    //tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    Tile *dTile = [tilesInPlaysort objectAtIndex:index4];
                    //NSLog(@"Checking the 4th tile");
                    if ([aTile isTileInSameRow:dTile] && aTile.colNum == dTile.colNum - 3)
                        //cTile.tileState == desiredTileState) {
                    {
                        NSString *tilenumber = [NSString stringWithFormat: @"%02d", dTile.tileType];
                        int value = [tilenumber intValue];
                        [self numbertoletterconvert:value];
                        if ([aTile isTileInSameRow:dTile] && [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]) {
                            // Add tiles to match array
                            //NSLog(@"I found a word");
                            
                            [self addTileToMatch:aTile];
                            [self addTileToMatch:bTile];
                            [self addTileToMatch:cTile];
                            [self addTileToMatch:dTile];
                            //[self animateTileRemoval:aTile];
                            //[self animateTileRemoval:bTile];
                            //[self animateTileRemoval:cTile];
                            //[self animateTileRemoval:dTile];
                            //NSLog(word);
                            lastWord = word;
                            [self updateLastWord];
                            [word setString:@""];
                            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                            break;
                        }
                        break;
                        
                        
                    }
                    else
                    {
                        [tilesInPlaysort removeObject:dTile];
                        //NSLog(@"%i", [tilesInPlaysort count]);
                        if ([tilesInPlaysort count] < 1){
                            break;
                        }
                        
                    }
                }
                
                break;      }
            [word setString:@""];
            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            break;
        }
    }
    
    //Collum Check

    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
    for (int index = [tilesInPlay count]-1; index>-1; index--) {
        Tile *aTile = [tilesInPlaysort objectAtIndex:index];
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        //NSLog(@"%@", aTile);
        for (int index2 = [tilesInPlay count]-1; index2>-1; index2--) {
            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            Tile *bTile = [tilesInPlaysort objectAtIndex:index2];
            if ([aTile isTileInSameColumn:bTile] &&
                aTile.rowNum == bTile.rowNum + 1 )
            {
                NSString *tilenumber = [NSString stringWithFormat: @"%02d", bTile.tileType];
                int value = [tilenumber intValue];
                [self numbertoletterconvert:value];
                tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            }
            else
            {
                [tilesInPlaysort removeObject:bTile];
                //NSLog(@"%i", [tilesInPlaysort count]);
                if ([tilesInPlaysort count] == 0){
                    [word setString:@""];
                    break;
                }
                
                continue;
            }
            for (int index3 = [tilesInPlay count]-1; index3>-1; index3--) {
                //tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                Tile *cTile = [tilesInPlaysort objectAtIndex:index3];
                if ([aTile isTileInSameColumn:cTile] &&
                    aTile.rowNum == cTile.rowNum + 2 )
                    // &&bTile.tileState == desiredTileState) {
                {
                    NSString *tilenumber = [NSString stringWithFormat: @"%02d", cTile.tileType];
                    int value = [tilenumber intValue];
                    [self numbertoletterconvert:value];
                    //[tilesInPlaysort removeAllObjects];
                    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    //NSLog(word);
                    //[word setString:@""];
                    // looking for a 3rd in a row
                }
                else
                {
                    [tilesInPlaysort removeObject:cTile];
                    //NSLog(@"%i", [tilesInPlaysort count]);
                    if ([tilesInPlaysort count] == 0){
                        [word setString:@""];
                        break;
                    }
                    
                    continue;
                    
                }
                for (int index4 = [tilesInPlay count]-1; index4>-1; index4--) {
                    //tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                    Tile *dTile = [tilesInPlaysort objectAtIndex:index4];
                    //NSLog(@"Checking the 4th tile");
                    if ([aTile isTileInSameColumn:dTile] && aTile.rowNum == dTile.rowNum + 3)
                        //cTile.tileState == desiredTileState) {
                    {
                        NSString *tilenumber = [NSString stringWithFormat: @"%02d", dTile.tileType];
                        int value = [tilenumber intValue];
                        [self numbertoletterconvert:value];
                        //[self reverseString:word];
                        if ([aTile isTileInSameColumn:dTile] && [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]) {
                            // Add tiles to match array
                            NSLog(@"I found a word");
                            
                            [self addTileToMatch:aTile];
                            [self addTileToMatch:bTile];
                            [self addTileToMatch:cTile];
                            [self addTileToMatch:dTile];
                            //[self animateTileRemoval:aTile];
                            //[self animateTileRemoval:bTile];
                            //[self animateTileRemoval:cTile];
                            //[self animateTileRemoval:dTile];
                            //NSLog(word);
                            lastWord = word;
                            [self updateLastWord];
                            [word setString:@""];
                            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
                            break;
                        }
                        break;
                        
                        
                    }
                    else
                    {
                        [tilesInPlaysort removeObject:dTile];
                        //NSLog(@"%i", [tilesInPlaysort count]);
                        if ([tilesInPlaysort count] < 1){
                            break;
                        }
                        
                    }
                }
                
                break;      }
            [word setString:@""];
            tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
            break;
        }
    }
            

        
        
}
    /*
    for (Tile *aTile in tilesInPlay) {
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        [self findTile:aTile.rowNum+1 andColumn: aTile.colNum];
        NSString *tilenumber2 = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value2 = [tilenumber2 intValue];
        [self numbertoletterconvert:value2];
        [self findTile:aTile.rowNum+2 andColumn: aTile.colNum];
        NSString *tilenumber3 = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value3 = [tilenumber3 intValue];
        [self numbertoletterconvert:value3];
        NSLog(word);
     
    }
    */


-(void)checkForMatchesOfType3:(TileType)desiredTileState {
    word = [[NSMutableString alloc]init];
    for (int ypos = 0; ypos<6; ypos++) {
        for (int xpos = 0; xpos<6; xpos++){
        Tile *aTile = [tilesInPlay objectAtIndex:((ypos*6+xpos))];
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
        if (xpos > 4)
        {
            [self checkForWord:word andRow:xpos andColumn:ypos];
        }
        
        //NSLog(@"@i @%", index, aTile);
        }
        [word setString:@""];
    }
    /*
    word = [[NSMutableString alloc]init];
    for (int index = [tilesInPlay count]-1; index>-1; index--) {
        //Tile *aTile = [tilesInPlay objectAtIndex:index];
        Tile *aTile = [[Tile alloc]init];
        aTile.rowNum = xpos;
        aTile.colNum = ypos;
        NSString *tilenumber = [NSString stringWithFormat: @"%02d", aTile.tileType];
        int value = [tilenumber intValue];
        [self numbertoletterconvert:value];
    }
     */
    
}

-(void)checkForWord:(NSMutableString*)word andRow:(NSInteger)xpos andColumn:(NSInteger)ypos{
    for (int dummy = 1; dummy < 2;dummy++){
    NSMutableString *tempword = word;
    NSString *sub1 = [word substringFromIndex:1];
    NSString *sub2 = [word substringFromIndex:2];
    NSString *sub3 = [word substringToIndex:4];
    NSString *sub4 = [word substringToIndex:5];
    NSString *sub5 = [word substringWithRange:NSMakeRange(1, 4)];
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(word)]) {
        NSLog(word);
        NSLog(@"Word Found");
        Tile *aTile = [tilesInPlay objectAtIndex:((ypos*6+xpos))];
        Tile *bTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-1))];
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        Tile *eTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-4))];
        Tile *fTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-5))];
        [self addTileToMatch:aTile];
        [self addTileToMatch:bTile];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        [self addTileToMatch:eTile];
        break;
    }
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(sub1)]) {
        NSLog(sub1);
        NSLog(@"Word Found");
        Tile *aTile = [tilesInPlay objectAtIndex:((ypos*6+xpos))];
        Tile *bTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-1))];
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        Tile *eTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-4))];
        [self addTileToMatch:aTile];
        [self addTileToMatch:bTile];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        [self addTileToMatch:eTile];
        break;
    }
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(sub2)]) {
        NSLog(sub2);
        NSLog(@"Word Found");
        Tile *aTile = [tilesInPlay objectAtIndex:((ypos*6+xpos))];
        Tile *bTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-1))];
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        [self addTileToMatch:aTile];
        [self addTileToMatch:bTile];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        break;
    }
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(sub3)]) {
        NSLog(sub3);
        NSLog(@"Word Found");
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        Tile *eTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-4))];
        Tile *fTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-5))];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        [self addTileToMatch:eTile];
        [self addTileToMatch:fTile];
        break;
    }
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(sub4)]) {
        NSLog(sub4);
        NSLog(@"Word Found");
        Tile *bTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-1))];
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        Tile *eTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-4))];
        [self addTileToMatch:bTile];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        [self addTileToMatch:eTile];
        break;
        
    }
    if ([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:(sub5)]) {
        NSLog(sub5);
        NSLog(@"Word Found");
        Tile *bTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-1))];
        Tile *cTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-2))];
        Tile *dTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-3))];
        Tile *eTile = [tilesInPlay objectAtIndex:((ypos*6+xpos-4))];
        [self addTileToMatch:bTile];
        [self addTileToMatch:cTile];
        [self addTileToMatch:dTile];
        [self addTileToMatch:eTile];
        break;
    }
    }
    
}



-(NSString*)reverseString:(NSString*)word
{
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [word length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[word substringWithRange:subStrRange]];
    }
    NSLog(@"%@", reversedString);
    return word;
    
}

#pragma mark Unused Dictionary Method

- (void) checkDictionary{
    
    word = [[NSMutableString alloc]init];
    //word = @"";
    
    NSInteger map[12][12];
    for (Tile *aTile in tilesInPlay) {
    //    if (aTile.tileState != kTileIdle) {
            // If tile is moving or scoring, fill with zero
      //      map[aTile.rowNum][aTile.colNum] = 0;
            //tilesInAction++;
      //  } else {
            map[aTile.rowNum][aTile.colNum] = aTile.tileType;
      //  }
   }
    for (int row = 1; row <= boardRows; row++) {
        for (int col = 1; col <= boardColumns; col++) {
            TileType a = map[row][col];
            TileType b = map[row][col+1];
            TileType c = map[row][col+2];
            TileType d = map[row][col+3];
            TileType e = map[row+1][col];
            TileType f = map[row+2][col];
            TileType g = map[row+3][col];
            
            //for (int count = 65)
            NSString *rawnumber = [NSMutableString stringWithFormat: @"%02d",a];
            int value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            rawnumber = [NSMutableString stringWithFormat: @"%02d",b];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            rawnumber = [NSMutableString stringWithFormat: @"%02d",c];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            //if([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]){
                //NSLog(@"Word Found");
                //NSLog(@"%i",row);
                //NSLog(@"%i",col);
                //[self addTileToMatch:[self findTile: row andColumn: col]];
                //[self addTileToMatch:[self findTile: row andColumn: col+1]];
                //[self addTileToMatch:[self findTile: row andColumn: col+2]];
            //}
            rawnumber = [NSString stringWithFormat: @"%02d",d];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            if([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]){
                NSLog(@"Word Found");
                [self addTileToMatch:[self findTile: row andColumn: col]];
                [self addTileToMatch:[self findTile: row andColumn: col+1]];
                [self addTileToMatch:[self findTile: row andColumn: col+2]];
                [self addTileToMatch:[self findTile: row andColumn: col+3]];
            }
            [word setString:@""];
            
            
            rawnumber = [NSString stringWithFormat: @"%02d",a];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            rawnumber = [NSString stringWithFormat: @"%02d",e];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            rawnumber = [NSString stringWithFormat: @"%02d",f];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            //if([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]){
            //    NSLog(@"Word Found");
            //    [self addTileToMatch:[self findTile: row andColumn: col]];
            //    [self addTileToMatch:[self findTile: row+1 andColumn: col]];
            //    [self addTileToMatch:[self findTile: row+2 andColumn: col]];
            //}
            rawnumber = [NSString stringWithFormat: @"%02d",g];
            value = [rawnumber intValue];
            [self numbertoletterconvert:value];
            if([UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:word]){
                NSLog(@"Word Found");
                [self addTileToMatch:[self findTile: row andColumn: col]];
                [self addTileToMatch:[self findTile: row+1 andColumn: col]];
                [self addTileToMatch:[self findTile: row+2 andColumn: col]];
                [self addTileToMatch:[self findTile: row+3 andColumn: col]];
            }
            [word setString:@""];
            
        }
        NSLog(@"********BREAK BETWEEN ROWS************");
    }
    NSLog(@"******************BREAK BETWEEN SWAPS**********************");
}


-(Tile*)findTile:(NSInteger)row andColumn:(NSInteger)col
{
    tilesInPlaysort = [NSMutableArray arrayWithArray:tilesInPlay];
    for (Tile *aTile in tilesInPlaysort) {
        if (aTile.colNum == col && aTile.rowNum == row){
            return aTile;
            break;
        }
        else [tilesInPlaysort removeObject:aTile];
    }
    return 0;
}


-(NSMutableString*)numbertoletterconvert:(int)value{
    switch(value) {
        case 01:
            //NSLog(@"I logged a A ");
            [word appendString:@"A"];
            break;
        case 02:
            //NSLog(@"I logged a B");
            [word appendString:@"B"];
            break;
        case 03:
            //NSLog(@"I logged a C");
            [word appendString:@"C"];
            break;
        case 04:
            //NSLog(@"I logged a D");
            [word appendString:@"D"];
            break;
        case 05:
            //NSLog(@"I logged a E");
            [word appendString:@"E"];
            break;
        case 06:
            //NSLog(@"I logged a F");
            [word appendString:@"F"];
            break;
        case 07:
            //NSLog(@"I logged a G");
            [word appendString:@"G"];
            break;
        case 8:
            //NSLog(@"I logged a H");
            [word appendString:@"H"];
            break;
        case 9:
            //NSLog(@"I logged a I");
            [word appendString:@"I"];
            break;
        case 10:
            //NSLog(@"I logged a J");
            [word appendString:@"J"];
            break;
        case 11:
            //NSLog(@"I logged a K");
            [word appendString:@"K"];
            break;
        case 12:
            //NSLog(@"I logged a L");
            [word appendString:@"L"];
            break;
        case 13:
            //NSLog(@"I logged a M");
            [word appendString:@"M"];
            break;
        case 14:
            //NSLog(@"I logged a N");
            [word appendString:@"N"];
            break;
        case 15:
            //NSLog(@"I logged a O");
            [word appendString:@"O"];
            break;
        case 16:
            //NSLog(@"I logged a P");
            [word appendString:@"P"];
            break;
        case 17:
            //NSLog(@"I logged a Q");
            [word appendString:@"Q"];
            break;
        case 18:
            //NSLog(@"I logged a R");
            [word appendString:@"R"];
            break;
        case 19:
            //NSLog(@"I logged a S");
            [word appendString:@"S"];
            break;
        case 20:
            //NSLog(@"I logged a T");
            [word appendString:@"T"];
            break;
        case 21:
            //NSLog(@"I logged a U");
            [word appendString:@"U"];
            break;
        case 22:
            //NSLog(@"I logged a V");
            [word appendString:@"V"];
            break;
        case 23:
            //NSLog(@"I logged a W");
            [word appendString:@"W"];
            break;
        case 24:
            //NSLog(@"I logged a X");
            [word appendString:@"X"];
            break;
        case 25:
            //NSLog(@"I logged a Y");
            [word appendString:@"Y"];
            break;
        case 26:
            //NSLog(@"I logged a Z");
            [word appendString:@"Z"];
            break;
        default:
            //NSLog(@"Out of bounds");
            //[word appendString:@"*"];
            [word setString:@""];
            break;
    }
    NSLog (word);
    return word;
}



-(void) addTileToMatch:(Tile*)thisTile {
    if ([tileMatches indexOfObject:thisTile] == NSNotFound & thisTile != nil) {
        [tileMatches addObject:thisTile];
    }
}

#pragma mark Positioning
-(CGPoint) positionForRow:(NSInteger)rowNum andColumn:(NSInteger)colNum {
    
    float x = boardOffsetWidth + ((tileSize.width + padWidth) * colNum);
    float y = boardOffsetHeight + ((tileSize.height + padHeight) * rowNum);
    
    return ccp(x,y);
}

-(CGPoint) scorePosition {
    return ccp(420, size.height - 50);
}

-(CGPoint) wordPosition {
    return ccp(420, size.height/2);
}

-(CGPoint) timerPosition {
    return ccp(420, size.height/6);
}

-(CGPoint) swapPosition {
    return ccp(420, size.height/3);
}

#pragma mark onEnter/onExit
-(void)onEnter
{
    [[[CCDirector sharedDirector] touchDispatcher]
     addTargetedDelegate:self priority:0
     swallowsTouches:YES];
    [super onEnter];
}


-(void)onExit
{
    [[[CCDirector sharedDirector] touchDispatcher]
     removeDelegate:self];
    
    [super onExit];
}


#pragma mark Touch Handlers
-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint location = [touch locationInView:[touch view]];
    CGPoint convLoc = [[CCDirector sharedDirector]
                       convertToGL:location];
    //touches to menu
    if (isGameOver) {
        [[CCDirector sharedDirector]
         replaceScene:[MenuScene scene]];
        return YES;
    }
    
    // back button was pressed -> exit

     if (CGRectContainsPoint([backButton boundingBox],
     convLoc)) {
     [[CCDirector sharedDirector]
     replaceScene:[MenuScene node]];
     return YES;
     }
    
    if (CGRectContainsPoint([bombButton boundingBox],
                            convLoc)) {
        if (bombPowerUp > 0) {
        for (Tile *aTile in tilesInPlay) {
            [self animateTileRemoval:aTile];
        }
            [self addTilesToFillBoard];
            [self checkMove];
        bombPowerUp--;
        [self updatePowerUpDisplay];
        }
    }
    
    if (CGRectContainsPoint([freezeButton boundingBox],
                            convLoc)) {
        if (freezePowerUp > 0 && isSuperSwapOn == NO && isTimerOn == YES) {
        isTimerOn = NO;
        freezePowerUp--;
        [self updatePowerUpDisplay];
            swapsLeft = 10;
            [self generateSwapsLeftDisplay];
        }
    }
    
    if (CGRectContainsPoint([swapButton boundingBox],
                            convLoc)) {
        if (swapPowerUp > 0 && isTimerOn == YES && isSuperSwapOn == NO){
        isSuperSwapOn = YES;
        swapPowerUp--;
        [self updatePowerUpDisplay];
            swapsLeft = 10;
        [self generateSwapsLeftDisplay];
        }
    }

    
    // If we have only 0 or 1 tile in tilesTouched, track
    if ([tilesTouched count] < 2) {
        // Check each tile
        for (Tile *aTile in tilesInPlay) {
            // If the tile was touched AND the tile is idle,
            // return YES to track the touch
            if ([aTile containsTouchLocation:convLoc] &&
                aTile.tileState == kTileIdle) {
                return YES;
            }
        }
    }
    
    // If we failed to find any good touch, return
    return NO;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    // Swipes here.
    [self touchHelper:touch withEvent:event];
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    // Taps here.
    [self touchHelper:touch withEvent:event];
}

-(void) touchHelper:(UITouch *)touch withEvent:(UIEvent *)event {
    // If we're already checking for a match, ignore
    if ([tilesTouched count] >= 2 || tilesMoving == YES) {
        return;
    }
    
    CGPoint location = [touch locationInView:[touch view]];
    CGPoint convLoc = [[CCDirector sharedDirector]
                       convertToGL:location];
    
    // Let's figure out which tile was touched (if any)
    for (Tile *aTile in tilesInPlay) {
        if ([aTile containsTouchLocation:convLoc] &&
            aTile.tileState == kTileIdle) {
            // We can't add the same tile twice
            if ([tilesTouched containsObject:aTile] == NO) {
                // add the tile to the array
                [tilesTouched addObject:aTile];
                [aTile highlightTile];
            }
        }
    }
    
    //swap tiles
    if ([tilesTouched count] >= 2) {
        Tile *aTile = [tilesTouched objectAtIndex:0];
        Tile *bTile = [tilesTouched objectAtIndex:1];
        
        if ([aTile isTileBeside:bTile]) {
            if (isSuperSwapOn == YES && swapsLeft > 0){
                swapsLeft--;
                [self updateSwapsLeft];
            }
            if(isTimerOn == NO && swapsLeft > 0) {
                swapsLeft--;
                [self updateSwapsLeft];
            }
            if (swapsLeft == 0)
            {
                [self removeChild:swapsLeftLabel cleanup:YES];
                isTimerOn = YES;
                isSuperSwapOn = NO;
            }
            
            //if (isSuperSwapOn == NO || isTimerOn == YES)[self removeChild:swapsLeftLabel cleanup:YES];
            
            [self swapTile:aTile withTile:bTile];
            
        } else {
            if (isSuperSwapOn == NO) {
                //not adjacent, drop the first tile
                [aTile stopHighlightTile];
                [tilesTouched removeObject:aTile];
                //isSuperSwapOn = NO;
                [self removeChild:swapsLeftLabel cleanup:YES];
            }
            if (isSuperSwapOn == YES){
                swapsLeft--;
                [self updateSwapsLeft];
                [self swapTile:aTile withTile:bTile];
            }
            if (swapsLeft == 0){
                [self removeChild:swapsLeftLabel cleanup:YES];
                isTimerOn = YES;
                isSuperSwapOn = NO;
            }
        }
    }
}

#pragma mark Debugging stuffs


-(void) generateTestingPlayfield {
    
    [self generateTileForRow:1 andColumn:1 ofType:23];
    [self generateTileForRow:1 andColumn:2 ofType:7];
    [self generateTileForRow:1 andColumn:3 ofType:19];
    [self generateTileForRow:1 andColumn:4 ofType:12];
    [self generateTileForRow:1 andColumn:5 ofType:15];
    [self generateTileForRow:1 andColumn:6 ofType:18];
    //[self generateTileForRow:1 andColumn:7 ofType:7];
    
    [self generateTileForRow:2 andColumn:1 ofType:14];
    [self generateTileForRow:2 andColumn:2 ofType:21];
    [self generateTileForRow:2 andColumn:3 ofType:15];
    [self generateTileForRow:2 andColumn:4 ofType:9];
    [self generateTileForRow:2 andColumn:5 ofType:12];
    [self generateTileForRow:2 andColumn:6 ofType:16];
    //[self generateTileForRow:2 andColumn:7 ofType:4];
    
    [self generateTileForRow:3 andColumn:1 ofType:18];
    [self generateTileForRow:3 andColumn:2 ofType:24];
    [self generateTileForRow:3 andColumn:3 ofType:15];
    [self generateTileForRow:3 andColumn:4 ofType:20];
    [self generateTileForRow:3 andColumn:5 ofType:16];
    [self generateTileForRow:3 andColumn:6 ofType:4];
    //[self generateTileForRow:3 andColumn:7 ofType:1];
    
    [self generateTileForRow:4 andColumn:1 ofType:8];
    [self generateTileForRow:4 andColumn:2 ofType:18];
    [self generateTileForRow:4 andColumn:3 ofType:5];
    [self generateTileForRow:4 andColumn:4 ofType:18];
    [self generateTileForRow:4 andColumn:5 ofType:9];
    [self generateTileForRow:4 andColumn:6 ofType:18];
    //[self generateTileForRow:4 andColumn:7 ofType:4];
    
    [self generateTileForRow:5 andColumn:1 ofType:19];
    [self generateTileForRow:5 andColumn:2 ofType:6];
    [self generateTileForRow:5 andColumn:3 ofType:20];
    [self generateTileForRow:5 andColumn:4 ofType:14];
    [self generateTileForRow:5 andColumn:5 ofType:4];
    [self generateTileForRow:5 andColumn:6 ofType:15];
    //[self generateTileForRow:5 andColumn:7 ofType:1];
    
    [self generateTileForRow:6 andColumn:1 ofType:8];
    [self generateTileForRow:6 andColumn:2 ofType:16];
    [self generateTileForRow:6 andColumn:3 ofType:5];
    [self generateTileForRow:6 andColumn:4 ofType:5];
    [self generateTileForRow:6 andColumn:5 ofType:5];
    [self generateTileForRow:6 andColumn:6 ofType:4];
    //[self generateTileForRow:6 andColumn:7 ofType:2];
    
    // Add the tiles to the layer
    for (Tile *aTile in tilesInPlay) {
        [aTile setTileState:kTileIdle];
        [matchsheet addChild:aTile];
    }
    
}


@end