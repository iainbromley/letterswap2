//
//  MenuLayer.m
//
//

#import "MenuLayer.h"

@implementation MenuLayer

-(id) init
{
    if( (self=[super init])) {
        
        CGSize wins = [[CCDirector sharedDirector] winSize];
        
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"LetterSwap" fontName:@"Verdana-Bold" fontSize:45];
        [label setColor:ccWHITE];
		label.position =  ccp( wins.width /2 , wins.height/2 );
		[self addChild: label];
        
        CCLabelTTF *startGameLbl = [CCLabelTTF labelWithString:@"Start Game" fontName:@"Verdana-Bold" fontSize:22];
        CCMenuItemLabel *startGameItem = [CCMenuItemLabel itemWithLabel:startGameLbl target:self selector:@selector(startGame)];
        CCMenu *startMenu = [CCMenu menuWithItems:startGameItem, nil];
        [startMenu setPosition:ccp(wins.width/2, wins.height/4)];
        [self addChild:startMenu];
	}
    
    return self;
}

-(void) startGame {
    [[CCDirector sharedDirector] replaceScene:[PlayfieldScene scene]];
}

@end
