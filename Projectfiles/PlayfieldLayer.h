//
//  PlayfieldLayer.h
//
//

#import <Foundation/Foundation.h>
#import "MenuScene.h"
#import "Tile.h"


@interface PlayfieldLayer : CCLayer {
    CGSize size; 
    
    CCSpriteBatchNode *matchsheet;
    
    CCSprite *backButton; // simple sprite control to leave the scene
    CCSprite *freezeButton;
    CCSprite *bombButton;
    CCSprite *swapButton;
    
    NSMutableArray *tilesInPlay; // tiles on the board
    NSMutableArray *tilesInPlaysort; // temp array to sort tilesinPlay
    NSMutableArray *temptilesInPlaysort; //temp array to sort sort array
    NSMutableArray *tileMatches; // matched tiles on the board
    NSMutableArray *tilesTouched; //tiles currently in "touch" mode
    
    NSInteger boardColumns;
    NSInteger boardRows; 
    NSMutableString *word; 
    float boardOffsetWidth; 
    float boardOffsetHeight; 
    float padWidth; 
    float padHeight;
    
    NSInteger totalTilesAvailable;
    
    CGSize tileSize; 
    
    BOOL checkMatches; 
    BOOL tilesMoving; 
    
    NSInteger movesRemaining; 
    
    NSInteger playerScore; 
    CCLabelTTF *scoreLabel;
    
    NSMutableString *lastWord;
    CCLabelTTF *wordLabel;
    
    NSInteger freezePowerUp;
    CCLabelTTF *freezePowerUpLabel;
    
    NSInteger bombPowerUp;
    CCLabelTTF *bombPowerUpLabel;
    
    NSInteger swapPowerUp;
    CCLabelTTF *swapPowerUpLabel;
    
    NSInteger swapsLeft;
    CCLabelTTF *swapsLeftLabel;
    
    CCProgressTimer *timerDisplay;
    float currentTimerValue; 
    float startingTimerValue;
    
    BOOL isTimerOn;
    BOOL isSuperSwapOn;
    BOOL isGameOver;

}

-(CGPoint) positionForRow:(NSInteger)rowNum andColumn:(NSInteger)colNum ;
-(CGPoint) scorePosition;
-(CGPoint) wordPosition;
-(CGPoint) timerPosition;
-(NSString*)numbertoletterconvert:(int)value;
-(void) addTileToMatch:(Tile*)thisTile;

@end




