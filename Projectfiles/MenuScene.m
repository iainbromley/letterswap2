//
//  MAMenuScene.m
//
//

#import "MenuScene.h"
#import "MenuLayer.h"

@implementation MenuScene

+(id) scene
{
    CCScene *scene = [CCScene node];
    
    MenuLayer *layer = [MenuLayer node];
    
    [scene addChild: layer];
    
    return scene;
}

-(id) init
{
	if( (self=[super init])) {
        
        MenuLayer *layer = [MenuLayer node];
        [self addChild: layer];
	}
	return self;
}

@end
