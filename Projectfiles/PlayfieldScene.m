//
//  MAPlayfieldScene.m
//
//

#import "PlayfieldScene.h"
#import "PlayfieldLayer.h"

@implementation PlayfieldScene

+(id) scene
{
    CCScene *scene = [CCScene node];
    
    PlayfieldLayer *layer = [PlayfieldLayer node];
    
    [scene addChild: layer];
    
    return scene;
}

-(id) init
{
	if( (self=[super init])) {
        
        PlayfieldLayer *layer = [PlayfieldLayer node];
        [self addChild: layer];
	}
	return self;
}

@end
