//
//  Tile.m
//
//

#import "Tile.h"

@implementation Tile

@synthesize rowNum = _rowNum;
@synthesize colNum = _colNum;
@synthesize tileType = _tileType;
@synthesize tileState = _tileState;

//@synthesize gameLayer;

-(BOOL) isTileSameAs:(Tile*)otherTile {
    return (self.tileType == otherTile.tileType);
}

-(BOOL) isTileInSameRow:(Tile*)otherTile {
    return (self.rowNum == otherTile.rowNum);
}

-(BOOL) isTileInSameColumn:(Tile*)otherTile {
    return (self.colNum == otherTile.colNum);
}

-(BOOL) isTileBeside:(Tile*)otherTile {
    if ([self isTileInSameRow:otherTile] &&
        ((self.colNum == otherTile.colNum - 1) ||
         (self.colNum == otherTile.colNum + 1))
        ) {
        return YES;
    }
    else if ([self isTileInSameColumn:otherTile] &&
             ((self.rowNum == otherTile.rowNum - 1) ||
              (self.rowNum == otherTile.rowNum + 1))
             ) {
        return YES;
    } else {
        return NO;
    }
}


-(void) highlightTile {
    // animation
    CCMoveBy *moveUp = [CCMoveBy actionWithDuration:0.1
                                           position:ccp(0,3)];
    CCMoveBy *moveDown = [CCMoveBy actionWithDuration:0.1
                                             position:ccp(0,-3)];
    CCSequence *moveAround = [CCSequence actions:moveUp,
                              moveDown, nil];
    CCRepeatForever *tileHop = [CCRepeatForever
                                actionWithAction:moveAround];
    
    [self runAction:tileHop];
}

-(void) stopHighlightTile {
    [self stopAllActions];
}


- (BOOL)containsTouchLocation:(CGPoint)pos
{
	return CGRectContainsPoint(self.boundingBox, pos);
}

-(void) dealloc {
    [self setGameLayer:nil];
}

@end